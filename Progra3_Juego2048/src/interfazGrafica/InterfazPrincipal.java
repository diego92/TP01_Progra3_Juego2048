package interfazGrafica;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JOptionPane;

import codigoDeNegocio.Juego;
import java.awt.Color;

public class InterfazPrincipal {

	private JFrame ventanaDelJuego;
	private Juego miJuego2048;
	private JPanel panelPrincipal;
	
	private JLabel[][] valores;
	private JPanel[][] pnlCuadrantes;
	
	private JPanel panelPuntaje;
	private JLabel contadorDePuntaje;
	private JLabel TituloPuntaje;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazPrincipal window = new InterfazPrincipal();
					window.ventanaDelJuego.setVisible(true);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfazPrincipal() {
		
		inicializarComponentesDelJuego();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void inicializarComponentesDelJuego() {  //la lista de metodos que voy a hacer
		
		inicializarJuego();
		inicializarFrame();
		inicializarPanel();
	}

	private void inicializarPuntaje() {
		
		panelPuntaje = new JPanel();
		contadorDePuntaje = new JLabel();
		contadorDePuntaje.setForeground(Color.BLACK);
		
		panelPuntaje.setSize(432, 67);
		panelPuntaje.setLocation(20, 483);
		panelPuntaje.setBackground(new Color(102, 153, 255));
		panelPuntaje.setLayout(null);
		
		contadorDePuntaje.setBounds(134, 7, 254, 50);
		contadorDePuntaje.setText("0");
		contadorDePuntaje.setFont(new Font("Impact", Font.PLAIN, 30));
		
		
		panelPuntaje.add(contadorDePuntaje);
		
		panelPrincipal.add(panelPuntaje);
		
		TituloPuntaje = new JLabel("Puntaje :");
		TituloPuntaje.setForeground(Color.BLACK);
		TituloPuntaje.setFont(new Font("Impact", Font.PLAIN, 30));
		TituloPuntaje.setBounds(20, 11, 111, 39);
		panelPuntaje.add(TituloPuntaje);
	}

	private void inicializarFrame(){ //Ventana Principal
		
		ventanaDelJuego = new JFrame();
		ventanaDelJuego.setVisible(true);
		ventanaDelJuego.setResizable(false);
		ventanaDelJuego.setTitle("Juego 2048");
		ventanaDelJuego.setSize(480, 600);
		ventanaDelJuego.setLocationRelativeTo(null);
		ventanaDelJuego.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ventanaDelJuego.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
				miJuego2048.mover(e);                      
				
				actualizarEstadoEnPantalla();
				
				verificoGanador();
				
			}

			private void verificoGanador() {
				
				miJuego2048.verificarSiGano();
				miJuego2048.enCasoDeLlenoElTableroDecidir();
				
				if(miJuego2048.hayGanador()==true ){
					JOptionPane.showMessageDialog(null, "Ganaste el juego!!!");
					
					System.exit(1);
				}
	
				if(miJuego2048.hayPerdedor()==true){
					
					miJuego2048.guardarPuntaje();
					miJuego2048.mostrarPuntaje();
					
					System.exit(1);
				}
			}

			private void actualizarEstadoEnPantalla() {
				
				for(int i=0; i<miJuego2048.tamañoDelTablero(); i++) {
					
					for(int j=0; j<miJuego2048.tamañoDelTablero(); j++) {
						
						if(miJuego2048.verValorDeCelda(i, j)!=0)
							valores[i][j].setText(Integer.toString(miJuego2048.verValorDeCelda(i, j)));
						else
							valores[i][j].setText(null);
						
						valores[i][j].setFont(new Font("Arial", Font.BOLD, miJuego2048.dameCelda(i, j).tamañoEnFrame()));						
						
						pnlCuadrantes[i][j].setBackground(miJuego2048.verColorDeCelda(i, j));
						pnlCuadrantes[i][j].repaint();
					}
				}
				
				contadorDePuntaje.setText(Integer.toString(miJuego2048.verPuntajeAcumulado()));
				
				panelPrincipal.repaint();
			}
		});
	}
	
	
	private void inicializarPanel() { //Panel que contiene los cuadrantes
		
		panelPrincipal = new JPanel();
		panelPrincipal.setBackground(Color.BLACK);
		panelPrincipal.setLayout(null);
		ventanaDelJuego.getContentPane().add(panelPrincipal);
		inicializarPuntaje();
		inicializarCuadrantes();		
	}
	
	private void inicializarJuego() { //Juego
		
		miJuego2048 = new Juego();
	}

	private void inicializarCuadrantes() { //Cuadrantes del juego que muestra valores
		
		pnlCuadrantes = new JPanel[miJuego2048.tamañoDelTablero()][miJuego2048.tamañoDelTablero()];
		valores = new JLabel[miJuego2048.tamañoDelTablero()][miJuego2048.tamañoDelTablero()];
		
		int y = 10;
		int x = 10;
		
		for(int i=0; i<miJuego2048.tamañoDelTablero(); i++) {
			
			for(int j=0; j<miJuego2048.tamañoDelTablero(); j++) {
				
				//Paneles que establecen el fondo del cuadrante
				pnlCuadrantes[i][j] = new JPanel();
				pnlCuadrantes[i][j].setSize(100, 100);
				pnlCuadrantes[i][j].setLocation(x, y);
				pnlCuadrantes[i][j].setBackground(miJuego2048.verColorDeCelda(i, j));
				pnlCuadrantes[i][j].setLayout(null);
				
				//Etiquetas que representan los valores del juego
				valores[i][j] = new JLabel();
				if(miJuego2048.verValorDeCelda(i, j)!=0)
					valores[i][j].setText(Integer.toString(miJuego2048.verValorDeCelda(i, j)));
				
				valores[i][j].setFont(new Font("Arial", Font.BOLD, miJuego2048.dameCelda(i, j).tamañoEnFrame()));
				valores[i][j].setBounds(40, 25, 50, 50);
				
				//Agrego las etiquetas a los cuadrantes para que se mantengan en posicion
				pnlCuadrantes[i][j].add(valores[i][j]);
				
				//Agrego los cuadrantes al cuadrante principal
				panelPrincipal.add(pnlCuadrantes[i][j]);
				x+=118;
			}
			
			x = 10;
			y += 112;
		}
	}
}