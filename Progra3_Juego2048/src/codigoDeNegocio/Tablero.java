package codigoDeNegocio;

import java.awt.Color;

public class Tablero {

	private Celda[][] _conjuntoDeCeldas;
	private int _maximoValor;
	private int _tamanio;
	
	public Tablero() {
		
		_tamanio = 4;
		_conjuntoDeCeldas = new Celda[_tamanio][_tamanio];
		_maximoValor = 0;
		
		for(int i=0; i<_tamanio; i++) {
			
			for(int j=0; j<_tamanio; j++) {
				
				_conjuntoDeCeldas[i][j] = new Celda();
			}
		}
	}
	
	public int tama�o() {
		return _tamanio;
	}
	
	public int maximo() {
		return _maximoValor;
	}
	
	public boolean celdaVaciaEn(int fila, int columna) {
		return _conjuntoDeCeldas[fila][columna].estaVacia();
	}
	
	public int verValorEn(int fila, int columna) {
		return _conjuntoDeCeldas[fila][columna].valorDeCelda();
	}
	
	public Celda verCelda(int fila, int columna) {
		return _conjuntoDeCeldas[fila][columna];
	}
	
	public Color verColorDeFondoEn(int fila, int columna) {
		return _conjuntoDeCeldas[fila][columna].colorDeCelda();
	}
	
	public void establecerValorEnCelda(int numeroAEstablecer, int fila, int columna) {
		_conjuntoDeCeldas[fila][columna].establecerValor(numeroAEstablecer);
		loComparoConELMayor(fila, columna);
	}

	private void loComparoConELMayor(int x, int y) {
		_maximoValor = _conjuntoDeCeldas[x][y].valorDeCelda()>_maximoValor ? _conjuntoDeCeldas[x][y].valorDeCelda() : _maximoValor;
	}
	
public static boolean tablerosIguales(Tablero tableroPrincipal, Tablero tableroAComparar){
		
		for(int i=0; i<tableroPrincipal.tama�o(); i++){
			
			for(int j=0; j<tableroAComparar.tama�o(); j++){
				
				if(tableroPrincipal.verValorEn(i, j)!=tableroAComparar.verValorEn(i, j))
					return false;
			}
		}
		
		return true;
	}
//ver si el tablero esta lleno
	public boolean tableroLleno(){// recorrer toda la matriz y ver si esta llena
		boolean tableroLleno=true;
		for(int fila=0;fila<tama�o();fila++){
			boolean filaLlena=true;
			for(int columna=0;columna<tama�o();columna++){
				filaLlena=filaLlena && verValorEn(fila, columna)!=0;
				}
			tableroLleno=tableroLleno&&filaLlena;
		}
		
		return tableroLleno;
	}
	
	//TOSTRING PARA VER EL TABLERO
	@Override
	public String toString() { 
			
		String tablero = "";
			
		for(int i=0; i<_tamanio; i++) {
				
			for(int j=0; j<_tamanio; j++) {
					
				tablero = tablero + "|" + _conjuntoDeCeldas[i][j].valorDeCelda() + "|";
			}
				
			tablero = tablero + "\n";
		}
			
		return tablero;
	}
}
