package codigoDeNegocio;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;

public class Juego {

	private Tablero _miTablero2048;
	private boolean _gano;
	private boolean _perdio;
	private int _puntaje;

	//Genero La Matriz Del Juego
	public Juego() {

		_miTablero2048 = new Tablero();
		_gano = false;
		_perdio=false;
		_puntaje=0;

		establecerAleatorio();
		
	}

	//Verifico Ganador
	public void verificarSiGano() {
		_gano = _miTablero2048.maximo() == 2048;
	} 

	public void establecerValor(int valor, int x, int y) {
		_miTablero2048.establecerValorEnCelda(valor, x, y);
	}

	public boolean hayGanador() {
		return _gano;
	}
	//para el game over
	public boolean hayPerdedor(){
		return _perdio;
	}
	//recolectar del puntaje
	public int verPuntajeAcumulado(){
		return _puntaje;
	}
	//para la interfaz
	public Celda dameCelda(int i, int j){
		return _miTablero2048.verCelda(i, j);
	}
	//decidir si queda o se va este metodo
	public int verValorDeCelda(int fila, int columna) {
		return _miTablero2048.verValorEn(fila, columna);
	}

	public Color verColorDeCelda(int fila, int columna) {
		return _miTablero2048.verColorDeFondoEn(fila, columna);
	}

	public int tama�oDelTablero() {
		return _miTablero2048.tama�o();
	}

	private void establecerAleatorio(){

		Random rnd = new Random();
		int x,y;

		x = rnd.nextInt(4);
		y = rnd.nextInt(4);
		while(!_miTablero2048.celdaVaciaEn(x, y)){
			x = rnd.nextInt(4);
			y = rnd.nextInt(4);
		}

		_miTablero2048.establecerValorEnCelda((rnd.nextInt(2)==0?2:4), x, y);

	}

	/////////////////////////////////////////////////
	// chequea si puede continuar o perdio por tablero lleno
	public void enCasoDeLlenoElTableroDecidir(){
		if(hayGanador()==false && _miTablero2048.tableroLleno()==true){
			if( ConVecinosDeIgualValor()==false ){
				_perdio=true; //gano sigue en false

				System.out.println("Game Over");

			}else{
				System.out.println("Movimiento disponible");
			}
		}

	}
	///////////////////////////////////////////////////////////////

	//mover (una fila) hacia la derecha
	public void moverFilaAlaDerecha(int fila) {   

		int[] arregloAuxiliar = new int[4];
		int cont = 0;

		for (int i = 0; i < _miTablero2048.tama�o(); i++) {
			if (_miTablero2048.celdaVaciaEn(fila, i)) {
				arregloAuxiliar[cont] = _miTablero2048.verValorEn(fila, i);
				cont++;
			}
		}

		for (int j = 0; j < _miTablero2048.tama�o(); j++) {
			if (_miTablero2048.celdaVaciaEn(fila, j)==false) {
				arregloAuxiliar[cont] = _miTablero2048.verValorEn(fila, j);
				cont++;
			}
		}

		for (int k = 0; k < arregloAuxiliar.length; k++) {
			_miTablero2048.establecerValorEnCelda(arregloAuxiliar[k], fila, k);
		}
	}

	//mover (una fila) hacia la izquierda
	public void moverFilaAlaIzquierda(int fila){
		int[]arregloAuxiliar=new int[4];
		int cont=0;
		for(int j=0;j<_miTablero2048.tama�o();j++){
			if(_miTablero2048.celdaVaciaEn(fila, j)==false){
				arregloAuxiliar[cont]=_miTablero2048.verValorEn(fila, j);
				cont++;
			}
		}
		for(int i=0;i<_miTablero2048.tama�o();i++){
			if(_miTablero2048.celdaVaciaEn(fila, i )){
				arregloAuxiliar[cont]=_miTablero2048.verValorEn(fila, i);
				cont++;
			}
		}

		for(int k=0;k<arregloAuxiliar.length;k++){
			_miTablero2048.establecerValorEnCelda(arregloAuxiliar[k], fila, k);

		}

	}

	//mover hacia arriba (una columna)
	public void moverColumnaHaciaArriba(int columna){
		int[]arregloAuxiliar=new int[4];
		int cont=0;
		for(int j=0;j<_miTablero2048.tama�o();j++){
			if(_miTablero2048.celdaVaciaEn(j, columna)==false){
				arregloAuxiliar[cont]=_miTablero2048.verValorEn(j, columna);
				cont++;
			}
		}
		for(int i=0;i<_miTablero2048.tama�o();i++){
			if(_miTablero2048.celdaVaciaEn(i, columna)){
				arregloAuxiliar[cont]=_miTablero2048.verValorEn(i, columna);
				cont++;
			}
		}
		for(int k=0;k<arregloAuxiliar.length;k++){
			_miTablero2048.establecerValorEnCelda(arregloAuxiliar[k],k, columna);

		}

	}

	// mover (la columna) hacia Abajo                     
	public void moverColumnaHaciaAbajo(int columna){
		int[]arregloAuxiliar=new int[4];
		int cont=0;
		for(int j=0;j<_miTablero2048.tama�o();j++){
			if(_miTablero2048.celdaVaciaEn(j,columna)){
				arregloAuxiliar[cont]=_miTablero2048.verValorEn(j, columna);
				cont++;
			}

		}
		for(int i=0;i<_miTablero2048.tama�o();i++){
			if(_miTablero2048.celdaVaciaEn(i,columna)==false){
				arregloAuxiliar[cont]=_miTablero2048.verValorEn(i, columna);
				cont++;
			}
		}

		for(int k=0;k<arregloAuxiliar.length;k++){
			_miTablero2048.establecerValorEnCelda(arregloAuxiliar[k],k, columna);

		}
	}

	// LA PARTE REFERIDA A LAS SUMAS DE UNA FILA O UNA COLUMNA
	public void sumarAlaDerecha(int fila){ 
		moverFilaAlaDerecha(fila);
		for(int i=_miTablero2048.tama�o()-1;i>0;i--){
			if(_miTablero2048.verValorEn(fila , i)==_miTablero2048.verValorEn(fila, i-1) && _miTablero2048.verValorEn(fila, i)!=0 ){
				_miTablero2048.establecerValorEnCelda(_miTablero2048.verValorEn(fila, i)*2, fila, i);
				_miTablero2048.establecerValorEnCelda(0, fila, i-1);
				_puntaje=_puntaje+_miTablero2048.verValorEn(fila, i);
			}
		}
		moverFilaAlaDerecha(fila);

	}	


	//sumar a la izquierda(una fila)                  
	public void sumarAlaIzquierda(int fila ){
		moverFilaAlaIzquierda(fila);
		for(int i=0;i<_miTablero2048.tama�o()-1;i++){
			if(_miTablero2048.verValorEn(fila, i)==_miTablero2048.verValorEn(fila,i+1) && _miTablero2048.verValorEn(fila,i)!=0 ){
				_miTablero2048.establecerValorEnCelda((_miTablero2048.verValorEn(fila,i))*2, fila, i);
				_miTablero2048.establecerValorEnCelda(0, fila, i+1);
				_puntaje=_puntaje+_miTablero2048.verValorEn(fila, i);

			}
		}
		moverFilaAlaIzquierda(fila);

	}

	//sumar columna hacia arriba(una columna)
	public void sumarColumnaHaciaArriba(int columna){ 
		this.moverColumnaHaciaArriba(columna);
		for(int j=0;j<_miTablero2048.tama�o()-1;j++){
			if(_miTablero2048.verValorEn(j , columna) == _miTablero2048.verValorEn(j+1, columna) && _miTablero2048.verValorEn(j , columna)!=0){
				_miTablero2048.establecerValorEnCelda(_miTablero2048.verValorEn(j+1, columna)*2, j, columna);
				_miTablero2048.establecerValorEnCelda(0, j+1 , columna);
				_puntaje=_puntaje+_miTablero2048.verValorEn(j, columna);
			}
		}
		this.moverColumnaHaciaArriba(columna);

	}

	//sumar hacia abajo(una columna)                        
	public void sumarColumnaHaciaAbajo(int columna){
		this.moverColumnaHaciaAbajo(columna);
		for(int j=_miTablero2048.tama�o()-1;j>0;j--){
			if(_miTablero2048.verValorEn(j,columna)==_miTablero2048.verValorEn(j-1,columna) && _miTablero2048.verValorEn(j,columna)!=0){
				_miTablero2048.establecerValorEnCelda(_miTablero2048.verValorEn(j-1,columna)*2,j,columna);
				_miTablero2048.establecerValorEnCelda(0,j-1,columna);
				_puntaje=_puntaje+_miTablero2048.verValorEn(j, columna);
			}
		}
		moverColumnaHaciaAbajo(columna);

	}

	// MOVER Y SUMAR TODOS LOS VALORES POSIBLES HACIA LA DERECHA          
	public void sumarDerechaMTX(){
		for(int i=0;i<_miTablero2048.tama�o();i++){
			sumarAlaDerecha(i);
		}


	}

	// MOVER Y SUMAR TODOS LOS VALORES POSIBLES HACIA LA IZQUIERDA		
	public void sumarIzquierdaMTX(){
		for(int j=0;j<_miTablero2048.tama�o();j++){
			sumarAlaIzquierda(j);
		}

	}

	//MOVER Y SUMAR TODOS LOS VALORES POSIBLES HACIA ABAJO
	public void sumarAbajoMTX(){						
		for(int l=0;l<_miTablero2048.tama�o();l++){
			sumarColumnaHaciaAbajo(l);
		}

	}

	//MOVER Y SUMAR TODOS LOS VALORES POSIBLES HACIA ARRIBA
	public void sumarArribaMTX(){					
		for(int k=0;k<_miTablero2048.tama�o();k++){
			this.sumarColumnaHaciaArriba(k);
		}

	}
	
	public boolean ConVecinosDeIgualValor(){// recorrer toda la matriz y chequear si existe un movimiento permitido posible
		ArrayList<ArrayList<Integer>> _vecinos;
		ArrayList<Integer> celdas;

		_vecinos= new ArrayList< ArrayList<Integer>>();
		celdas=new ArrayList<Integer>();

		for(int fila=0;fila<_miTablero2048.tama�o();fila++){  //cargo el arraylist con los valores de cada posicion de las 16 celdas
			for(int columna=0;columna<_miTablero2048.tama�o();columna++){
				celdas.add(_miTablero2048.verValorEn(fila, columna));	
			}
		}

		for(int i=0; i<celdas.size();i++){ //creo las listas
			_vecinos.add(new ArrayList<Integer>());	
		}

		int cont=0;
		for(int fila=0;fila<_miTablero2048.tama�o();fila++){  // inicializo los conjuntos
			for(int columna=0;columna<_miTablero2048.tama�o();columna++){
				if( columna-1>=0 && columna-1<_miTablero2048.tama�o() && fila>=0 && fila<_miTablero2048.tama�o()){
					_vecinos.get(cont).add(_miTablero2048.verValorEn(fila, columna-1));

				}
				if(columna+1>=0 && columna+1<_miTablero2048.tama�o() && fila>=0 && fila<_miTablero2048.tama�o()){
					_vecinos.get(cont).add(_miTablero2048.verValorEn(fila, columna+1));

				}
				if(fila-1>=0 && fila-1<_miTablero2048.tama�o() && columna>=0 && columna<_miTablero2048.tama�o()){ 
					_vecinos.get(cont).add(_miTablero2048.verValorEn(fila-1, columna));

				}
				if(fila+1>=0 && fila+1<_miTablero2048.tama�o() && columna>=0 && columna<_miTablero2048.tama�o()){ 
					_vecinos.get(cont).add(_miTablero2048.verValorEn(fila+1, columna));

				}
				cont++;	
			}
		}
		for(int m=0;m<celdas.size();m++){
			if(_vecinos.get(m).contains(celdas.get(m))){
				return true;
			}
		}
		return false;
	}

	// Movimientos del Tablero segun se aprietan las teclas
	public void mover(KeyEvent e) {
		Tablero estadoDeTablero = new Tablero();
		for(int i=0; i<estadoDeTablero.tama�o(); i++){
			for(int j=0; j<estadoDeTablero.tama�o(); j++){
				estadoDeTablero.establecerValorEnCelda(_miTablero2048.verValorEn(i, j), i, j);
			}
		}

		if(e.getKeyCode()==KeyEvent.VK_UP)
			sumarArribaMTX();
		if(e.getKeyCode()==KeyEvent.VK_DOWN)
			sumarAbajoMTX();
		if(e.getKeyCode()==KeyEvent.VK_LEFT)
			sumarIzquierdaMTX();
		if(e.getKeyCode()==KeyEvent.VK_RIGHT)
			sumarDerechaMTX();

		if(!Tablero.tablerosIguales(_miTablero2048, estadoDeTablero))
			establecerAleatorio();
	}

	public void guardarPuntaje() {

		String nombreJugador = JOptionPane.showInputDialog(null, "Ingrese nombre: ");
		
		if(nombreJugador.length()!=0)
			guardarPuntaje(_puntaje + " " + nombreJugador);
	}
	
	private void guardarPuntaje(String puntajeYNombre){
		
		try {
			FileWriter guardandoRecords = new FileWriter("puntajeDejugadores.txt", true);
			
			for(int i=0; i<puntajeYNombre.length(); i++) {
				
				guardandoRecords.write(puntajeYNombre.charAt(i));
			}
			
			guardandoRecords.write('\n');
			
			guardandoRecords.close();
			
		} catch (IOException e) {
			System.out.println("No se Encontro el archivo de puntajes");
		}
	}
	
	public void mostrarPuntaje() {
		
		try {
			FileReader leyendoRecords = new FileReader("puntajeDejugadores.txt");
			
			int c = leyendoRecords.read();
			String muestroRecords = "";
			while(c!=-1) {				
				
				muestroRecords = muestroRecords+(char)c;
				
				c = leyendoRecords.read();
			}
			
			JOptionPane.showMessageDialog(null, "Puntajes Maximos:\n"+muestroRecords);
			
			leyendoRecords.close();
		} catch (IOException e) {
			System.out.println("No se encontro el archivo de puntajes");
		}
	}
}
