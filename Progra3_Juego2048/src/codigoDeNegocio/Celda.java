package codigoDeNegocio;

import java.awt.Color;

public class Celda{
	
	private int _valor;
	private Color _colorcitoDeFondo;
	
	//CONSTRUCTORES
	public Celda() {
		this(0);
		establecerColor();
	}
	
	public Celda(int valorEnCelda) {
		_valor = valorEnCelda;
		establecerColor();
	}
	
	//VERIFICA SI LA CELDA TIENE UN CERO O UN VALOR
	public Boolean estaVacia() {
		return _valor == 0 ? true : false;
	}
	
	//GETTERS Y SETTER
	public int valorDeCelda() {
		return _valor;
	}
	
	public Color colorDeCelda() {
		return _colorcitoDeFondo;
	}
	
	public void establecerValor(int numero) {
		_valor = numero;
		establecerColor();
	}
	
	public int tamañoEnFrame(){
		
		if(_valor>=100 && _valor<=999)
			return 20;
		if(_valor>=1000 && _valor<=9999)
			return 18;
		
		return 25;
	}
	
	public boolean esIgual(Celda celdaAcomparar){
		
		if(_valor == celdaAcomparar._valor)
			return true;
		
		return false;
	}
	private void establecerColor() {
		
		switch (_valor) {
		case 2: _colorcitoDeFondo =  new Color(232,187,121);
				break;
		case 4: _colorcitoDeFondo = new Color(219,217,75);
				break;
		case 8: _colorcitoDeFondo = new Color(165,204,49);
				break;
		case 16: _colorcitoDeFondo = new Color(117,223,235);
				break;
		case 32: _colorcitoDeFondo = new Color(97,180,232);
				break;
		case 64: _colorcitoDeFondo = new Color(131,174,242);
				break;
		case 128: _colorcitoDeFondo = new Color(144,125,219);
				break;
		case 256: _colorcitoDeFondo = new Color(212,114,237);
				break;
		case 512: _colorcitoDeFondo = new Color(212,169,205);
				break;
		case 1024: _colorcitoDeFondo = new Color(232,105,70);
				break;
		case 2048: _colorcitoDeFondo = new Color(218,240,22);
		break;
		
		default:
			_colorcitoDeFondo = Color.DARK_GRAY;
			break;
		}
		
	}
}
