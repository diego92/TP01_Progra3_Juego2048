package test;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import codigoDeNegocio.Juego;

public class JuegoTest {
	

	@Test
	public void juegoGanoTest() {
		Juego juego= new Juego();
		juego.establecerValor(2048, 1, 1);
		juego.verificarSiGano();
		assertTrue(juego.hayGanador());
	}
	@Test
	public void juegoNoGanoTest() {
		Juego juego= new Juego();
		assertFalse(juego.hayGanador());
	}
	@Test
	public void noGanoConValorMaximo(){
		Juego juego= new Juego();
		juego.establecerValor(1024, 1, 1);
		juego.verificarSiGano();
		assertFalse(juego.hayGanador());
	}

	@Test
	public void establecerValorTest() {
		Juego juego= new Juego();
		juego.establecerValor(5, 0, 0);
		assertTrue(juego.verValorDeCelda(0, 0) == 5);
	}
	
	
	@Test
	public void verValorTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		assertTrue(juego.verValorDeCelda(0, 0) == 0);
	}
	
	// Testeo de color
	@Test
	public void verColorCeroTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		assertTrue(juego.verColorDeCelda(0, 0) == Color.DARK_GRAY);
	}
	@Test
	public void verColorDosTest() {
		Juego juego= new Juego();
		juego.establecerValor(2, 1, 0);
		assertTrue(juego.verColorDeCelda(1, 0) != Color.DARK_GRAY);
	}
	
	@Test
	public void tamaņoDelTableroTest() {
		Juego juego= new Juego();
		assertTrue(juego.tamaņoDelTablero() == 4);
	}
	
	// Testeo de mover
	@Test
	public void moverFilaDerechaTest() {
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(8, 0, 2);
		juego.establecerValor(16, 0, 3);
		juego.moverFilaAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==2 && juego.verValorDeCelda(0, 1)==4 && juego.verValorDeCelda(0, 2)==8 && juego.verValorDeCelda(0, 3)==16);
	}
	@Test
	public void moverFilaDerechaConCerosTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(16, 0, 3);
		juego.moverFilaAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==4 && juego.verValorDeCelda(0, 3)==16);
	}
	@Test
	public void moverFilaDerechaCerosTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.moverFilaAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	@Test
	public void moverFilaDerechaUnCeroTest() {
		Juego juego= new Juego();
		juego.establecerValor(8, 0, 0);
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(16, 0, 3);
		juego.moverFilaAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==8 && juego.verValorDeCelda(0, 2)==4 && juego.verValorDeCelda(0, 3)==16);
	}
	@Test
	public void moverFilaDerechaUnoDsitintoDeCeroTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 0, 1);
		juego.establecerValor(16, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.moverFilaAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==16);
	}
	
	// mover izquierda
	@Test
	public void moverFilaIzquierdaTest() {
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(8, 0, 2);
		juego.establecerValor(16, 0, 3);
		juego.moverFilaAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==2 && juego.verValorDeCelda(0, 1)==4 && juego.verValorDeCelda(0, 2)==8 && juego.verValorDeCelda(0, 3)==16);
	}
	@Test
	public void moverFilaIzquierdaConCerosTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(16, 0, 3);
		juego.moverFilaAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==4 && juego.verValorDeCelda(0, 1)==16 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	@Test
	public void moverFilaIzquierdaCerosTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.moverFilaAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	@Test
	public void moverFilaIzquierdaUnCeroTest() {
		Juego juego= new Juego();
		juego.establecerValor(8, 0, 0);
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(16, 0, 3);
		juego.moverFilaAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==8 && juego.verValorDeCelda(0, 1)==4 && juego.verValorDeCelda(0, 2)==16 && juego.verValorDeCelda(0, 3)==0);
	}
	@Test
	public void moverFilaIzquierdaUnoDsitintoDeCeroTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 0, 1);
		juego.establecerValor(16, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.moverFilaAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==16 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	//mover arriba
	@Test
	public void moverColumnaArribaTest() {
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(4, 1, 0);
		juego.establecerValor(8, 2, 0);
		juego.establecerValor(16, 3, 0);
		juego.moverColumnaHaciaArriba(0);
		assertTrue(juego.verValorDeCelda(0, 0)==2 && juego.verValorDeCelda(1, 0)==4 && juego.verValorDeCelda(2, 0)==8 && juego.verValorDeCelda(3, 0)==16);
	}
	@Test
	public void moverColumnaArribaConCerosTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(4, 1, 0);
		juego.establecerValor(0, 2, 0);
		juego.establecerValor(16, 3, 0);
		juego.moverColumnaHaciaArriba(0);
		assertTrue(juego.verValorDeCelda(0, 0)==4 && juego.verValorDeCelda(1, 0)==16 && juego.verValorDeCelda(2, 0)==0 && juego.verValorDeCelda(3, 0)==0);
	}
	@Test
	public void moverColumnaArribaCerosTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 1, 0);
		juego.establecerValor(0, 2, 0);
		juego.establecerValor(0, 3, 0);
		juego.moverColumnaHaciaArriba(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(1, 0)==0 && juego.verValorDeCelda(2, 0)==0 && juego.verValorDeCelda(3, 0)==0);
	}
	@Test
	public void moverColumnaArribadaUnCeroTest() {
		Juego juego= new Juego();
		juego.establecerValor(8, 0, 0);
		juego.establecerValor(4, 1, 0);
		juego.establecerValor(0, 2, 0);
		juego.establecerValor(16, 3, 0);
		juego.moverColumnaHaciaArriba(0);
		assertTrue(juego.verValorDeCelda(0, 0)==8 && juego.verValorDeCelda(1, 0)==4 && juego.verValorDeCelda(2, 0)==16 && juego.verValorDeCelda(3, 0)==0);
	}
	@Test
	public void moverColumnaArribaUnoDsitintoDeCeroTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 1, 0);
		juego.establecerValor(16, 2, 0);
		juego.establecerValor(0, 3, 0);
		juego.moverColumnaHaciaArriba(0);
		assertTrue(juego.verValorDeCelda(0, 0)==16 && juego.verValorDeCelda(1, 0)==0 && juego.verValorDeCelda(2, 0)==0 && juego.verValorDeCelda(3, 0)==0);
	}
	//mover Abajo
	@Test
	public void moverColumnaAbajoTest() {
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(4, 1, 0);
		juego.establecerValor(8, 2, 0);
		juego.establecerValor(16, 3, 0);
		juego.moverColumnaHaciaAbajo(0);
		assertTrue(juego.verValorDeCelda(0, 0)==2 && juego.verValorDeCelda(1, 0)==4 && juego.verValorDeCelda(2, 0)==8 && juego.verValorDeCelda(3, 0)==16);
	}
	public void moverColumnaAbajoConCerosTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(4, 1, 0);
		juego.establecerValor(0, 2, 0);
		juego.establecerValor(16, 3, 0);
		juego.moverColumnaHaciaAbajo(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(1, 0)==0 && juego.verValorDeCelda(2, 0)==4 && juego.verValorDeCelda(3, 0)==16);
	}
	@Test
	public void moverColumnaAbajoCerosTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 1, 0);
		juego.establecerValor(0, 2, 0);
		juego.establecerValor(0, 3, 0);
		juego.moverColumnaHaciaAbajo(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(1, 0)==0 && juego.verValorDeCelda(2, 0)==0 && juego.verValorDeCelda(3, 0)==0);
	}
	@Test
	public void moverColumnaAbajoDaUnCeroTest() {
		Juego juego= new Juego();
		juego.establecerValor(8, 0, 0);
		juego.establecerValor(4, 1, 0);
		juego.establecerValor(0, 2, 0);
		juego.establecerValor(16, 3, 0);
		juego.moverColumnaHaciaAbajo(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(1, 0)==8 && juego.verValorDeCelda(2, 0)==4 && juego.verValorDeCelda(3, 0)==16);
	}
	@Test
	public void moverColumnaAbajoUnoDsitintoDeCeroTest() {
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 1, 0);
		juego.establecerValor(16, 2, 0);
		juego.establecerValor(0, 3, 0);
		juego.moverColumnaHaciaAbajo(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(1, 0)==0 && juego.verValorDeCelda(2, 0)==0 && juego.verValorDeCelda(3, 0)==16);
	}
	// sumas a la derecha en una fila
	@Test
	public void sumarAlaDerechaFilaTest(){
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(8, 0, 2);
		juego.establecerValor(16, 0, 3);
		juego.sumarAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==2 && juego.verValorDeCelda(0, 1)==4 && juego.verValorDeCelda(0, 2)==8 && juego.verValorDeCelda(0, 3)==16);
	}
	@Test
	public void sumarAlaDerechaFilaDeCerosTest(){
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.sumarAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	public void sumarAlaDerechaFilaDosNumerosCercanosTest(){
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(2, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.sumarAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==4);
	}
	public void sumarAlaDerechaFilaDosNumerosAlternosTest(){
		Juego juego= new Juego();
		juego.establecerValor(4, 0, 0);
		juego.establecerValor(0, 0, 1);
		juego.establecerValor(4, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.sumarAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==8);
	}
	public void sumarAlaDerechaFilaDosNumerosAlternosCambioLugarTest(){
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(4, 0, 3);
		juego.sumarAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==8);
	}
	public void sumarAlaDerechaFilaDosParesDeNumerosTest(){
		Juego juego= new Juego();
		juego.establecerValor(8, 0, 0);
		juego.establecerValor(8, 0, 1);
		juego.establecerValor(4, 0, 2);
		juego.establecerValor(4, 0, 3);
		juego.sumarAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==16 && juego.verValorDeCelda(0, 3)==8);
	}
	public void sumarAlaDerechaFilaTodosIgualesTest(){
		Juego juego= new Juego();
		juego.establecerValor(8, 0, 0);
		juego.establecerValor(8, 0, 1);
		juego.establecerValor(8, 0, 2);
		juego.establecerValor(8, 0, 3);
		juego.sumarAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==16 && juego.verValorDeCelda(0, 3)==16);
	}
	public void sumarAlaDerechaFilaUnoAisladoTest(){
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 0, 1);
		juego.establecerValor(8, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.sumarAlaDerecha(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==8);
	}
	// sumas a la izquierda en una fila
	@Test
	public void sumarAlaIzquierdaFilaTest(){
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(8, 0, 2);
		juego.establecerValor(16, 0, 3);
		juego.sumarAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==2 && juego.verValorDeCelda(0, 1)==4 && juego.verValorDeCelda(0, 2)==8 && juego.verValorDeCelda(0, 3)==16);
	}
	@Test
	public void sumarAlaIzquierdaFilaDeCerosTest(){
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.sumarAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==0 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	@Test
	public void sumarAlaIzquierdaFilaDosNumerosCercanosTest(){
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(2, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.sumarAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==4 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	@Test
	public void sumarAlaIzquierdaFilaDosNumerosAlternosTest(){
		Juego juego= new Juego();
		juego.establecerValor(4, 0, 0);
		juego.establecerValor(0, 0, 1);
		juego.establecerValor(4, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.sumarAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==8 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	@Test
	public void sumarAlaIzquierdaFilaDosNumerosAlternosCambioLugarTest(){
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(4, 0, 3);
		juego.sumarAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==8 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	@Test
	public void sumarAlaIzquierdaFilaDosParesDeNumerosTest(){
		Juego juego= new Juego();
		juego.establecerValor(8, 0, 0);
		juego.establecerValor(8, 0, 1);
		juego.establecerValor(4, 0, 2);
		juego.establecerValor(4, 0, 3);
		juego.sumarAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==16 && juego.verValorDeCelda(0, 1)==8 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	@Test
	public void sumarAlaIzquierdaFilaTodosIgualesTest(){
		Juego juego= new Juego();
		juego.establecerValor(8, 0, 0);
		juego.establecerValor(8, 0, 1);
		juego.establecerValor(8, 0, 2);
		juego.establecerValor(8, 0, 3);
		juego.sumarAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==16 && juego.verValorDeCelda(0, 1)==16 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	@Test
	public void sumarAlaIzquierdaFilaUnoAisladoTest(){
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 0);
		juego.establecerValor(0, 0, 1);
		juego.establecerValor(8, 0, 2);
		juego.establecerValor(0, 0, 3);
		juego.sumarAlaIzquierda(0);
		assertTrue(juego.verValorDeCelda(0, 0)==8 && juego.verValorDeCelda(0, 1)==0 && juego.verValorDeCelda(0, 2)==0 && juego.verValorDeCelda(0, 3)==0);
	}
	//sumar columna hacia arriba
	@Test
	public void sumarColumnaDosIgualesArribaTest(){
		Juego juego= new Juego();
		juego.establecerValor(4, 0, 0);
		juego.establecerValor(2, 1, 0);
		juego.establecerValor(2, 2, 0);
		juego.establecerValor(16, 3, 0);
		juego.sumarColumnaHaciaArriba(0);
		assertTrue(juego.verValorDeCelda( 0 , 0)==4 && juego.verValorDeCelda(1, 0)==4 && juego.verValorDeCelda(2, 0)==16);
	}
	@Test 
	public void sumarColumnaTodosDistintosArribaTest(){
		Juego juego= new Juego();
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(2, 1, 1);
		juego.establecerValor(32, 2, 1);
		juego.establecerValor(16, 3, 1);
		juego.sumarColumnaHaciaArriba(1);
		assertTrue( juego.verValorDeCelda(0, 1)==4 && juego.verValorDeCelda(1, 1)==2 && juego.verValorDeCelda(2, 1)==32 && juego.verValorDeCelda(3, 1)==16 );
	}
	
	@Test 
	public void sumarColumnaTodosIgualesArribaTest(){
		Juego juego= new Juego();
		juego.establecerValor(8, 0, 2);
		juego.establecerValor(8, 1, 2);
		juego.establecerValor(8, 2, 2);
		juego.establecerValor(8, 3, 2);
		juego.sumarColumnaHaciaArriba(2);
		assertTrue( juego.verValorDeCelda(0, 2)==16 && juego.verValorDeCelda(1, 2)==16 );
	}
	@Test
	public void sumarColumnaDosNumerosAlternosCambioLugarArribaTest(){
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 3);
		juego.establecerValor(4, 1, 3);
		juego.establecerValor(0, 2, 3);
		juego.establecerValor(4, 3, 3);
		juego.sumarColumnaHaciaArriba(3);
		assertTrue(juego.verValorDeCelda(0, 3)==8 );
	}
	
	@Test
	public void sumarArribaExtremosTest(){
		Juego juego= new Juego();
		juego.establecerValor(16, 0, 3);
		juego.establecerValor(0, 1, 3);
		juego.establecerValor(0, 2, 3);
		juego.establecerValor(16, 3, 3);
		juego.sumarColumnaHaciaArriba(3);
		assertTrue(juego.verValorDeCelda(0, 3)==32 );
	}
	
	//sumar columna hacia Abajo
	@Test
	public void sumarColumnaDosIgualesAbajoTest(){
		Juego juego= new Juego();
		juego.establecerValor(8, 0, 0);
		juego.establecerValor(2, 1, 0);
		juego.establecerValor(2, 2, 0);
		juego.establecerValor(16, 3, 0);
		juego.sumarColumnaHaciaAbajo(0);
		assertTrue( juego.verValorDeCelda(1, 0)==8 && juego.verValorDeCelda(2, 0)==4 && juego.verValorDeCelda(3, 0)==16 );
	}
	@Test 
	public void sumarColumnaTodosDistintosAbajoTest(){
		Juego juego= new Juego();
		juego.establecerValor(4, 0, 1);
		juego.establecerValor(2, 1, 1);
		juego.establecerValor(32, 2, 1);
		juego.establecerValor(16, 3, 1);
		juego.sumarColumnaHaciaAbajo(1);
		assertTrue( juego.verValorDeCelda(0, 1)==4 && juego.verValorDeCelda(1, 1)==2 && juego.verValorDeCelda(2, 1)==32 && juego.verValorDeCelda(3, 1)==16 );
	}
	@Test 
	public void sumarColumnaTodosIgualesAbajoTest(){
		Juego juego= new Juego();
		juego.establecerValor(8, 0, 2);
		juego.establecerValor(8, 1, 2);
		juego.establecerValor(8, 2, 2);
		juego.establecerValor(8, 3, 2);
		juego.sumarColumnaHaciaAbajo(2);
		assertTrue( juego.verValorDeCelda(2, 2)==16 && juego.verValorDeCelda(3, 2)==16 );
	}
	@Test
	public void sumarColumnaDosNumerosAlternosCambioLugarAbajoTest(){
		Juego juego= new Juego();
		juego.establecerValor(0, 0, 3);
		juego.establecerValor(4, 1, 3);
		juego.establecerValor(0, 2, 3);
		juego.establecerValor(4, 3, 3);
		juego.sumarColumnaHaciaAbajo(3);
		assertTrue(juego.verValorDeCelda(3, 3)==8 );
	}
	@Test
	public void sumarAbajoExtremosTest(){
		Juego juego= new Juego();
		juego.establecerValor(16, 0, 3);
		juego.establecerValor(0, 1, 3);
		juego.establecerValor(0, 2, 3);
		juego.establecerValor(16, 3, 3);
		juego.sumarColumnaHaciaAbajo(3);
		assertTrue(juego.verValorDeCelda(3, 3)==32 );
	}
	// sumar la matriz hacia la derecha
	@Test
	public void sumarMatrizAlaDerechaTest(){
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(0, 0, 1);          //puedo colocarlos o no a los ceros
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(2, 0, 3);
		
		juego.establecerValor(0, 1, 0);
		juego.establecerValor(4, 1, 1);
		juego.establecerValor(4, 1, 2);
		juego.establecerValor(0, 1, 3);
		
		juego.establecerValor(8, 2, 0);
		juego.establecerValor(8, 2, 1);
		juego.establecerValor(2, 2, 2);
		juego.establecerValor(0, 2, 3);
		
		juego.establecerValor(4, 3, 0);
		juego.establecerValor(4, 3, 1);
		juego.establecerValor(8, 3, 2);
		juego.establecerValor(8, 3, 3);
		juego.sumarDerechaMTX();
		assertTrue(juego.verValorDeCelda(0, 3)==4 && juego.verValorDeCelda(1, 3)==8 && juego.verValorDeCelda(2, 2)==16 && juego.verValorDeCelda(2, 3)==2 && juego.verValorDeCelda(3, 2)==8 && juego.verValorDeCelda(3, 3)==16 );
	}
	//sumar la matriz hacia la izquierda
	@Test
	public void sumarMatrizAlaIzquierdaTest(){
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(0, 0, 1);          //puedo colocarlos o no
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(2, 0, 3);
		
		juego.establecerValor(0, 1, 0);
		juego.establecerValor(4, 1, 1);
		juego.establecerValor(4, 1, 2);
		juego.establecerValor(0, 1, 3);
		
		juego.establecerValor(8, 2, 0);
		juego.establecerValor(2, 2, 1);
		juego.establecerValor(8, 2, 2);
		juego.establecerValor(0, 2, 3);
		
		juego.establecerValor(4, 3, 0);
		juego.establecerValor(4, 3, 1);
		juego.establecerValor(8, 3, 2);
		juego.establecerValor(8, 3, 3);
		juego.sumarIzquierdaMTX();
		assertTrue(juego.verValorDeCelda(0, 0)==4 && juego.verValorDeCelda(1, 0)==8 && juego.verValorDeCelda(2, 0)==8 && juego.verValorDeCelda(2, 1)==2 && juego.verValorDeCelda(2, 2)==8 && juego.verValorDeCelda(3, 0)==8 && juego.verValorDeCelda(3, 1)==16);
	}
	
	//sumar la matriz hacia arriba
	@Test
	public void sumarMatrizArribaTest(){
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(0, 0, 1);          
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(2, 0, 3);
		
		juego.establecerValor(0, 1, 0);
		juego.establecerValor(4, 1, 1);
		juego.establecerValor(4, 1, 2);
		juego.establecerValor(0, 1, 3);
		
		juego.establecerValor(2, 2, 0);
		juego.establecerValor(2, 2, 1);
		juego.establecerValor(8, 2, 2);
		juego.establecerValor(0, 2, 3);
		
		juego.establecerValor(4, 3, 0);
		juego.establecerValor(4, 3, 1);
		juego.establecerValor(8, 3, 2);
		juego.establecerValor(2, 3, 3);
		juego.sumarArribaMTX();
		assertTrue(juego.verValorDeCelda(0, 0)==4 && juego.verValorDeCelda(1, 0)==4 && juego.verValorDeCelda(0, 1)==4 && juego.verValorDeCelda(1, 1)==2 && juego.verValorDeCelda(2, 1)==4 && juego.verValorDeCelda(0, 2)==4 && juego.verValorDeCelda(1, 2)==16 && juego.verValorDeCelda(0, 3)==4 );
	}
	//sumar matriz hacia abajo
	@Test
	public void sumarMatrizAbajoTest(){
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(0, 0, 1);          
		juego.establecerValor(0, 0, 2);
		juego.establecerValor(2, 0, 3);
		
		juego.establecerValor(0, 1, 0);
		juego.establecerValor(4, 1, 1);
		juego.establecerValor(4, 1, 2);
		juego.establecerValor(0, 1, 3);
		
		juego.establecerValor(2, 2, 0);
		juego.establecerValor(2, 2, 1);
		juego.establecerValor(8, 2, 2);
		juego.establecerValor(0, 2, 3);
		
		juego.establecerValor(4, 3, 0);
		juego.establecerValor(4, 3, 1);
		juego.establecerValor(8, 3, 2);
		juego.establecerValor(2, 3, 3);
		juego.sumarAbajoMTX();
		assertTrue(juego.verValorDeCelda(1, 1)==4 && juego.verValorDeCelda(2, 0)==4 && juego.verValorDeCelda(2, 1)==2 && juego.verValorDeCelda(2, 2)==4 && juego.verValorDeCelda(3, 0)==4 && juego.verValorDeCelda(3, 1)==4 && juego.verValorDeCelda(3, 2)==16 && juego.verValorDeCelda(3, 3)==4 );
		
	}
	
	@Test
	public void noSePuedeMover(){
		Juego juego= new Juego();
		juego.establecerValor(1, 0, 0);
		juego.establecerValor(2, 0, 1);          
		juego.establecerValor(3, 0, 2);
		juego.establecerValor(4, 0, 3);
		
		juego.establecerValor(5, 1, 0);
		juego.establecerValor(6, 1, 1);
		juego.establecerValor(7, 1, 2);
		juego.establecerValor(8, 1, 3);
		
		juego.establecerValor(9, 2, 0);
		juego.establecerValor(10, 2, 1);
		juego.establecerValor(11, 2, 2);
		juego.establecerValor(12, 2, 3);
		
		juego.establecerValor(13, 3, 0);
		juego.establecerValor(14, 3, 1);
		juego.establecerValor(15, 3, 2);
		juego.establecerValor(16, 3, 3);
		assertFalse(juego.ConVecinosDeIgualValor());
	}
	
	@Test
	public void sePuedeMover(){
		Juego juego= new Juego();
		juego.establecerValor(1, 0, 0);
		juego.establecerValor(2, 0, 1);          
		juego.establecerValor(3, 0, 2);
		juego.establecerValor(4, 0, 3);
		
		juego.establecerValor(5, 1, 0);
		juego.establecerValor(6, 1, 1);    //6 y posicion 10 son iguales
		juego.establecerValor(7, 1, 2);
		juego.establecerValor(8, 1, 3);
		
		juego.establecerValor(9, 2, 0);
		juego.establecerValor(6, 2, 1);
		juego.establecerValor(11, 2, 2);   // 11 y posicion 15 son iguales
		juego.establecerValor(12, 2, 3);
		
		juego.establecerValor(13, 3, 0);
		juego.establecerValor(14, 3, 1);
		juego.establecerValor(11, 3, 2);  
		juego.establecerValor(16, 3, 3);
		assertTrue(juego.ConVecinosDeIgualValor());
	}
	
	@Test
	public void existenMovimientosPosiblesMAG(){//vecinos en fila 0 o 3
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(5, 0, 1);          
		juego.establecerValor(4, 0, 2);
		juego.establecerValor(0, 0, 3);
		
		juego.establecerValor(8, 1, 0);
		juego.establecerValor(6, 1, 1);
		juego.establecerValor(7, 1, 2);
		juego.establecerValor(5, 1, 3);
		
		juego.establecerValor(2, 2, 0);
		juego.establecerValor(3, 2, 1);///
		juego.establecerValor(8, 2, 2);
		juego.establecerValor(5, 2, 3);
		
		juego.establecerValor(1, 3, 0);
		juego.establecerValor(3, 3, 1);///
		juego.establecerValor(9, 3, 2);
		juego.establecerValor(2, 3, 3);
		assertTrue(juego.ConVecinosDeIgualValor()==true);
	}
	@Test
	public void existenMovimientosPosibles2MAG(){/// vecinos en filas 1 y 2
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(3, 0, 1);          
		juego.establecerValor(4, 0, 2);
		juego.establecerValor(0, 0, 3);
		
		juego.establecerValor(8, 1, 0);///
		juego.establecerValor(16, 1, 1);
		juego.establecerValor(7, 1, 2);
		juego.establecerValor(5, 1, 3);
		
		juego.establecerValor(8, 2, 0);///
		juego.establecerValor(9, 2, 1);
		juego.establecerValor(0, 2, 2);
		juego.establecerValor(6, 2, 3);
		
		juego.establecerValor(1, 3, 0);
		juego.establecerValor(3, 3, 1);
		juego.establecerValor(9, 3, 2);
		juego.establecerValor(2, 3, 3);
		assertTrue(juego.ConVecinosDeIgualValor()==true);
	}
	@Test
	public void existenMovimientosPosibles3MAG(){//vecinos en misma fila
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(3, 0, 1);          
		juego.establecerValor(4, 0, 2);
		juego.establecerValor(0, 0, 3);
		
		juego.establecerValor(9, 1, 0);///
		juego.establecerValor(9, 1, 1);///
		juego.establecerValor(7, 1, 2);
		juego.establecerValor(5, 1, 3);
		
		juego.establecerValor(7, 2, 0);
		juego.establecerValor(9, 2, 1);
		juego.establecerValor(0, 2, 2);
		juego.establecerValor(6, 2, 3);
		
		juego.establecerValor(1, 3, 0);
		juego.establecerValor(3, 3, 1);
		juego.establecerValor(9, 3, 2);
		juego.establecerValor(2, 3, 3);
		assertTrue(juego.ConVecinosDeIgualValor()==true);
	}
	@Test
	public void sinMovimientosPosiblesMAG(){
		Juego juego= new Juego();
		juego.establecerValor(2, 0, 0);
		juego.establecerValor(3, 0, 1);          
		juego.establecerValor(4, 0, 2);
		juego.establecerValor(0, 0, 3);
		
		juego.establecerValor(3, 1, 0);
		juego.establecerValor(4, 1, 1);
		juego.establecerValor(32, 1, 2);
		juego.establecerValor(5, 1, 3);
		
		juego.establecerValor(16, 2, 0);
		juego.establecerValor(2, 2, 1);
		juego.establecerValor(16, 2, 2);
		juego.establecerValor(32, 2, 3);
		
		juego.establecerValor(4, 3, 0);
		juego.establecerValor(6, 3, 1);
		juego.establecerValor(8, 3, 2);
		juego.establecerValor(20, 3, 3);
		assertTrue(juego.ConVecinosDeIgualValor()==false);
	}

}
