package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import codigoDeNegocio.Tablero;

public class TableroTest {
	
	@Test
	public void tableroPorDefectoTest() {
		Tablero miTablero = new Tablero();
		assertTrue(miTablero.tama�o() == 4);
	}
	
	@Test
	public void valorMaximoTest() {
		Tablero miTablero = new Tablero();
		miTablero.establecerValorEnCelda(1024, 2, 3);
		
		assertTrue(miTablero.maximo()==1024);
	}
	
	@Test
	public void celdaVaciaTest() {
		Tablero miTablero = new Tablero();
		miTablero.establecerValorEnCelda(1024, 2, 2);
		
		assertFalse(miTablero.celdaVaciaEn(2, 2));
	}
	
	@Test
	public void verValorTest() {
		Tablero miTablero = new Tablero();
		miTablero.establecerValorEnCelda(512, 3, 2);
		
		assertTrue(miTablero.verValorEn(3, 2) == 512);
	}
	
	// verifica si el tablero esta lleno
	@Test
	public void tableroLleno(){
		Tablero juego= new Tablero();
			juego.establecerValorEnCelda(2, 0, 0);
			juego.establecerValorEnCelda(3, 0, 1);          
			juego.establecerValorEnCelda(4, 0, 2);
			juego.establecerValorEnCelda(2, 0, 3);
				
			juego.establecerValorEnCelda(2, 1, 0);
			juego.establecerValorEnCelda(4, 1, 1);
			juego.establecerValorEnCelda(4, 1, 2);
			juego.establecerValorEnCelda(5, 1, 3);
				
			juego.establecerValorEnCelda(2, 2, 0);
			juego.establecerValorEnCelda(2, 2, 1);
			juego.establecerValorEnCelda(8, 2, 2);
			juego.establecerValorEnCelda(5, 2, 3);
				
			juego.establecerValorEnCelda(4, 3, 0);
			juego.establecerValorEnCelda(3, 3, 1);
			juego.establecerValorEnCelda(8, 3, 2);
			juego.establecerValorEnCelda(2, 3, 3);

			juego.tableroLleno();
			assertTrue( juego.tableroLleno() );
			}
			
	@Test
	public void tableroConCeldaLibre(){
		Tablero juego= new Tablero();
		juego.establecerValorEnCelda(2, 0, 0);
		juego.establecerValorEnCelda(3, 0, 1);          
		juego.establecerValorEnCelda(4, 0, 2);
		juego.establecerValorEnCelda(0, 0, 3);
				
		juego.establecerValorEnCelda(8, 1, 0);
		juego.establecerValorEnCelda(4, 1, 1);
		juego.establecerValorEnCelda(4, 1, 2);
		juego.establecerValorEnCelda(5, 1, 3);
			
		juego.establecerValorEnCelda(2, 2, 0);
		juego.establecerValorEnCelda(2, 2, 1);
		juego.establecerValorEnCelda(8, 2, 2);
		juego.establecerValorEnCelda(5, 2, 3);
		
		juego.establecerValorEnCelda(1, 3, 0);
		juego.establecerValorEnCelda(3, 3, 1);
		juego.establecerValorEnCelda(8, 3, 2);
		juego.establecerValorEnCelda(2, 3, 3);
		juego.tableroLleno();
		assertFalse( juego.tableroLleno() );
	}

}
