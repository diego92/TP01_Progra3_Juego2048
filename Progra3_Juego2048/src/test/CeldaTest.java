package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.Test;

import codigoDeNegocio.Celda;

public class CeldaTest {

	@Test
	public void celdaVaciaTest() {
		
		Celda miCelda = new Celda();
		assertTrue(miCelda.estaVacia());
	}
	
	@Test
	public void celdaConValorTest() {
		
		Celda miCelda = new Celda(16);
		assertFalse(miCelda.estaVacia());
	}
	
	@Test
	public void establecerValorCeldaTest() {
		
		Celda miCelda = new Celda(16);
		miCelda.establecerValor(99);
		
		assertTrue(miCelda.valorDeCelda()==99);
	}
	
	@Test
	public void valorDeCeldaTest() {
		
		Celda miCelda = new Celda(32);
		assertTrue(miCelda.valorDeCelda() == 32);
	}
	
	@Test
	public void colorDeFondoPorDefectoTest() {
		
		Celda miCelda = new Celda();
		assertTrue(miCelda.colorDeCelda() == Color.DARK_GRAY);
	}
	
	@Test
	public void colorDeFondoModificadoPorValorTest() {
		
		Celda miCelda = new Celda();
		miCelda.establecerValor(2);
		
		assertTrue(miCelda.colorDeCelda() != Color.GRAY);
		
	}
	
	@Test
	public void tamanioEnFrameTest() {
		
		Celda miCelda = new Celda();
		miCelda.establecerValor(100);
		
		assertTrue(miCelda.tamañoEnFrame()==20);
	}
}
